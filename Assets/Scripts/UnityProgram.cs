﻿using UnityEngine;
using Cubertería;

public class UnityProgram : MonoBehaviour
{
    static Cubierto[] cubiertos;
    void Start()
    {
        cubiertos = new Cubierto[6];
        cubiertos[0] = new Cuchara("Cuchara sopera", Cuchara.Type.Cuchara_sopera, "LIMPIO");
        cubiertos[1] = new Cuchara("Cuchara de ensala", Cuchara.Type.Cuchara_de_ensalada, "LIMPIO");
        cubiertos[2] = new Tenedor("Tenedor de mesa", Tenedor.Type.Tenedor_de_mesa, "LIMPIO");
        cubiertos[3] = new Tenedor("Tenedor de ensala", Tenedor.Type.Tenedor_de_ensalada, "LIMPIO");
        cubiertos[4] = new Cuchillo("Cuchillo de mesa", Cuchillo.Type.Cuchillo_de_mesa, "LIMPIO");
        cubiertos[5] = new Cuchillo("Cuchillo de ensala", Cuchillo.Type.Cuchillo_de_carne, "LIMPIO");

        Loop();
    }
    void Loop()
    {
        Debug.Log("Cubiertos");
        Debug.Log("---------");

        string str = "1";
        int option = int.Parse(str);
        if (option < 7 && cubiertos[option - 1].state == "LIMPIO")
        {
            cubiertos[option - 1].state = "SUCIO";
        }
        else if (option < 7 && cubiertos[option - 1].state == "SUCIO")
        {
            cubiertos[option - 1].state = "LIMPIO";
        }

        for (int i = 0; i < cubiertos.Length; i++)
            Debug.Log("\t" + (1 + i) + " - " + cubiertos[i].name + " --- Estado: " + cubiertos[i].state);
    }
}