﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cubertería;

namespace ConsoleApp1
{
    class Program
    {
        static Cubertería.Cubierto[] cubiertos;
        static void Main(string[] args)
        {
            bool loop = true;
            cubiertos = new Cubertería.Cubierto[6];
            cubiertos[0] = new Cuchara("Cuchara sopera", Cuchara.Type.Cuchara_sopera, "LIMPIO");
            cubiertos[1] = new Cuchara("Cuchara de ensala", Cuchara.Type.Cuchara_de_ensalada, "LIMPIO");
            cubiertos[2] = new Tenedor("Tenedor de mesa", Tenedor.Type.Tenedor_de_mesa, "LIMPIO");
            cubiertos[3] = new Tenedor("Tenedor de ensala", Tenedor.Type.Tenedor_de_ensalada, "LIMPIO");
            cubiertos[4] = new Cuchillo("Cuchillo de mesa", Cuchillo.Type.Cuchillo_de_mesa, "LIMPIO");
            cubiertos[5] = new Cuchillo("Cuchillo de ensala", Cuchillo.Type.Cuchillo_de_carne, "LIMPIO");

            while (loop)
            {
                Console.Clear();

                Loop();
            }
        }
        static void Loop()
        {
            Console.WriteLine("Cubiertos");
            Console.WriteLine("---------");

            for (int i = 0; i < cubiertos.Length; i++)
                Console.WriteLine("\t" + (1 + i) + " - " + cubiertos[i].name + " --- Estado: " + cubiertos[i].state);


            string str = Console.ReadLine();
            int option = Int32.Parse(str);
            if (option < 7 && cubiertos[option - 1].state == "LIMPIO")
            {
                cubiertos[option - 1].state = "SUCIO";
            }
            else if (option < 7 && cubiertos[option - 1].state == "SUCIO")
            {
                cubiertos[option - 1].state = "LIMPIO";
            }
        }
    }
}