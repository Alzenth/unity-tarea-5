﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubertería
{
    class Cubierto
    {
        public string name;
        public string state;
        public Cubierto(string name, string state)
        {
            this.name = name;
            this.state = state;
        }
    }
}