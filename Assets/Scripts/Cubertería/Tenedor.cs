﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubertería
{
    class Tenedor : Cubierto
    {
        public enum Type
        {
            Tenedor_de_mesa,
            Tenedor_de_ensalada
        }
        public Type type;
        public Tenedor(string name, Type type, string state) : base(name, state)
        {
            this.type = type;
        }
        public void HincaComida()
        {

        }
    }
}