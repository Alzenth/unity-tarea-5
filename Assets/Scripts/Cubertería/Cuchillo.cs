﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubertería
{
    class Cuchillo : Cubierto
    {
        public enum Type
        {
            Cuchillo_de_mesa,
            Cuchillo_de_carne
        }
        public Type type;
        public Cuchillo(string name, Type type, string state) : base(name, state)
        {
            this.type = type;
        }
        public void CortaComida()
        {

        }
    }
}