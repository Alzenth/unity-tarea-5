﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubertería
{
    class Cuchara : Cubierto
    {
        public enum Type
        {
            Cuchara_sopera,
            Cuchara_de_ensalada
        }
        public Type type;
        public Cuchara(string name, Type type, string state) : base(name, state)
        {
            this.type = type;
        }
        public void RecogeComida()
        {

        }
    }
}